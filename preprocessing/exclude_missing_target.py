import os
import json
import warnings
import numpy as np
import pandas as pd

import logging

logger = logging.getLogger(__name__)


class ExcludeRowsMissingTarget(object):
    @staticmethod
    def transform(X=None, y=None, warn=False):
        if y is None:
            return X, y, sample_weight
        y_non_missing = y.notnull()
        if y.isnull().sum().compute() == 0:
            return X, y, sample_weight
        logger.debug("Exclude rows with missing target values")
        if warn:
            warnings.warn(
                """There are samples with missing target values in the data 
                which will be excluded for further analysis"""
                )
        y = y[y_non_missing]

        if X is not None:
            X = X[y_non_missing]
        
        return X, y
