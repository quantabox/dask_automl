import numpy as np

from dask_ml.model_selection import train_test_split
from dask import dataframe as dd
from preprocessing.preprocessing import Preprocessing

df = dd.read_csv("C:\\Users\\AG02628\\OneDrive - Anthem\\Documents\\Git\\dask_automl\\tests\\data\\dataset_44_spambase.csv", header=0) 
X, y = (df.drop(["class"], axis=1),  df["class"])
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, shuffle=True, random_state=1234)


m = Preprocessing().fit_and_transform(X_train, y_train)